package GUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

import Stores.*;

public class Controller {
	private GUI g;
	private Queue[] queues;
	private Store s;
	private BufferedWriter out;
	
	public Controller(GUI g) {
		this.g = g;
		this.g.addStartListener(new Start());
		this.g.addStopListener(new Stop());
		this.g.addWaitListener(new Wait());
		this.g.addPeakListener(new Peak());
		this.g.addServiceListener(new Service());
		this.g.addEmptyListener(new Empty());
	}
	
	public class Start implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			try {
				out = new BufferedWriter(new FileWriter("events.txt"));
				int queueNo = Integer.parseInt(g.getQueueNo());
				int simB = Integer.parseInt(g.getSimB());
				int simE = Integer.parseInt(g.getSimE());
				if(simE < simB)
					throw new NumberFormatException();
				int servB = Integer.parseInt(g.getServB());
				int servE = Integer.parseInt(g.getServiceE());
				if(servE < servB)
					throw new NumberFormatException();
				int pauseB = Integer.parseInt(g.getPauseB());
				int pauseE = Integer.parseInt(g.getPauseE());
				if(pauseE < pauseB)
					throw new NumberFormatException();
				int threshold = Integer.parseInt(g.getThreshold());
				queues = new Queue[queueNo];
				for(int i = 0; i < queueNo; i++)
					queues[i] = new Queue(i + 1, simE - simB, simB, out);
		        s = new Store(queues, servB, servE, pauseB, pauseE, simB, simE, threshold, out);
		        for(int i = 0; i < queueNo; i++)
					queues[i].start();
		        s.start();
				g.setWaitState(true);
				g.setEmptyState(true);
				g.setPeakeState(true);
				g.setServiceState(true);
		        g.setStartState(false);
		        VisualQueues vq = new VisualQueues(s.getQueues(), g);
		        vq.start();
			} catch (IOException e) {
				e.printStackTrace();
			} catch(NumberFormatException e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
			
	       
		}
	}
	
	public class Wait implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOutput("The average waiting time is: " + s.averageWaiting());
		}
	}
	
	public class Peak implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOutput("The peak time was: " + s.getPeakTime() + "\nThe maximum number of clients was: " + s.getPeakClients());
		}
	}
	
	public class Service implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOutput("The average service time was: " + s.averageService());
		}
	}
	
	public class Empty implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			String s = new String("");
			for(int i = 0; i < queues.length; i++)
			{
				s = s.concat("Queue " + i + " was empty for: " + queues[i].getEmptyTime() + "\n");
			}
			g.setOutput(s);
		}
	}
	
	public class Stop implements ActionListener
	{
		@SuppressWarnings("deprecation")
		public void actionPerformed(ActionEvent arg0) {
			for(int i = 0; i < queues.length; i++)
				queues[i].stop();
			s.stop();
		}
	}
	
	
}
