package GUI;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class GUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private final JPanel panel = new JPanel();
	private final JButton btnWait = new JButton("Average waiting time");
	private final JButton btnService = new JButton("Average service time");
	private final JButton btnPeak = new JButton("Peak time");
	private final JButton btnEmpty = new JButton("Empty time");
	private final JButton btnStart;
	private JTextArea textArea;
	private JTextArea textArea_1;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JButton btnStop;
	private JScrollPane scrollPane_1;
	
	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public GUI() {
		setTitle("Queues simulator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1583, 1337);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		btnWait.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnWait.setFont(new Font("Tahoma", Font.PLAIN, 44));
		
		
		btnWait.setBounds(893, 863, 459, 54);
		panel.add(btnWait);
		btnService.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnService.setFont(new Font("Tahoma", Font.PLAIN, 44));
		btnService.setBounds(290, 863, 459, 54);
		
		panel.add(btnService);
		btnPeak.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnPeak.setFont(new Font("Tahoma", Font.PLAIN, 44));
		btnPeak.setBounds(377, 945, 290, 60);
		
		panel.add(btnPeak);
		btnEmpty.setFont(new Font("Tahoma", Font.PLAIN, 44));
		btnEmpty.setBounds(985, 945, 290, 60);
		
		panel.add(btnEmpty);
		
		JLabel lblResult = new JLabel("Result:");
		lblResult.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblResult.setBounds(0, 1056, 227, 69);
		panel.add(lblResult);
		
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 44));
		textArea.setColumns(10);
		textArea.setBounds(189, 0, 1166, 54);
		panel.add(textArea);
		
		textArea_1 = new JTextArea();
		textArea_1.setLineWrap(true);
		textArea_1.setFont(new Font("Monospaced", Font.PLAIN, 44));
		textArea_1.setColumns(10);
		textArea_1.setBounds(652, 233, 863, 593);
		panel.add(textArea_1);
		
		textField = new JTextField();
		textField.setText("10");
		textField.setFont(new Font("Tahoma", Font.PLAIN, 44));
		textField.setBounds(251, 171, 341, 80);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setText("40");
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 44));
		textField_1.setColumns(10);
		textField_1.setBounds(251, 265, 341, 80);
		panel.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setText("5");
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 44));
		textField_2.setColumns(10);
		textField_2.setBounds(251, 362, 341, 80);
		panel.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setText("10");
		textField_3.setFont(new Font("Tahoma", Font.PLAIN, 44));
		textField_3.setColumns(10);
		textField_3.setBounds(251, 457, 341, 80);
		panel.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setText("0");
		textField_4.setFont(new Font("Tahoma", Font.PLAIN, 44));
		textField_4.setColumns(10);
		textField_4.setBounds(251, 548, 341, 80);
		panel.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setText("3");
		textField_5.setFont(new Font("Tahoma", Font.PLAIN, 44));
		textField_5.setColumns(10);
		textField_5.setBounds(251, 74, 341, 80);
		panel.add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setText("7");
		textField_6.setFont(new Font("Tahoma", Font.PLAIN, 44));
		textField_6.setColumns(10);
		textField_6.setBounds(251, 639, 341, 80);
		panel.add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setText("8");
		textField_7.setFont(new Font("Tahoma", Font.PLAIN, 44));
		textField_7.setColumns(10);
		textField_7.setBounds(251, 736, 341, 80);
		panel.add(textField_7);
		
		JLabel lblQueuesNo = new JLabel("Queues no.:");
		lblQueuesNo.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblQueuesNo.setBounds(0, 80, 248, 69);
		panel.add(lblQueuesNo);
		
		JLabel lblSimBegin = new JLabel("Sim. begin:");
		lblSimBegin.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblSimBegin.setBounds(0, 182, 248, 69);
		panel.add(lblSimBegin);
		
		JLabel lblSimEnd = new JLabel("Sim. end:");
		lblSimEnd.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblSimEnd.setBounds(0, 276, 248, 69);
		panel.add(lblSimEnd);
		
		JLabel lblServiceMin = new JLabel("Service min:");
		lblServiceMin.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblServiceMin.setBounds(0, 361, 248, 69);
		panel.add(lblServiceMin);
		
		JLabel lblServiceMax = new JLabel("Service max:");
		lblServiceMax.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblServiceMax.setBounds(0, 468, 269, 69);
		panel.add(lblServiceMax);
		
		JLabel lblPauseMin = new JLabel("Pause min:");
		lblPauseMin.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblPauseMin.setBounds(0, 554, 248, 69);
		panel.add(lblPauseMin);
		
		JLabel lblPauseMax = new JLabel("Pause max:");
		lblPauseMax.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblPauseMax.setBounds(0, 650, 248, 69);
		panel.add(lblPauseMax);
		
		JLabel lblWaitingThershold = new JLabel("Threshold:");
		lblWaitingThershold.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblWaitingThershold.setBounds(0, 747, 248, 60);
		panel.add(lblWaitingThershold);
		
		JLabel lblQueuesEvolution = new JLabel("Queues evolution:");
		lblQueuesEvolution.setFont(new Font("Tahoma", Font.PLAIN, 44));
		lblQueuesEvolution.setBounds(908, 136, 361, 69);
		panel.add(lblQueuesEvolution);
		
		btnStart = new JButton("START ");
		btnStart.setFont(new Font("Tahoma", Font.PLAIN, 44));
		btnStart.setBounds(850, 62, 227, 60);
		panel.add(btnStart);
		
		btnStop = new JButton("STOP");
		btnStop.setFont(new Font("Tahoma", Font.PLAIN, 44));
		btnStop.setBounds(1105, 62, 227, 60);
		panel.add(btnStop);
		
		btnWait.setEnabled(false);
		btnService.setEnabled(false);
		btnPeak.setEnabled(false);
		btnEmpty.setEnabled(false);
		
		scrollPane_1 = new JScrollPane(textArea);
		scrollPane_1.setBounds(136, 1033, 1354, 185);
		panel.add(scrollPane_1);
		
	}
	
	public String getSimB()
	{
		return textField.getText();
	}
	
	public String getSimE()
	{
		return textField_1.getText();
	}
	
	public String getServB()
	{
		return textField_2.getText();
	}
	
	public String getServiceE()
	{
		return textField_3.getText();
	}
	
	public String getPauseB()
	{
		return textField_4.getText();
	}
	
	public String getPauseE()
	{
		return textField_6.getText();
	}
	
	public String getThreshold()
	{
		return textField_7.getText();
	}
	
	public String getQueueNo()
	{
		return textField_5.getText();
	}
	
	public void setOutput(String s)
	{
		textArea.setText(s);
	}
	
	public void setQueueEv(String s)
	{
		textArea_1.setText(s);
	}
	
	public void addServiceListener(ActionListener a)
	{
		btnService.addActionListener(a);
	}
	
	
	public void addPeakListener(ActionListener a)
	{
		btnPeak.addActionListener(a);
	}
	
	public void addEmptyListener(ActionListener a)
	{
		btnEmpty.addActionListener(a);
	}
	
	public void addStartListener(ActionListener a)
	{
		btnStart.addActionListener(a);
	}
	
	public void addWaitListener(ActionListener a) {
		btnWait.addActionListener(a);
	}
	
	public void setStartState(boolean b)
	{
		btnStart.setEnabled(b);
	}
	
	public void setWaitState(boolean b)
	{
		btnWait.setEnabled(b);
	}
	
	public void setServiceState(boolean b)
	{
		btnService.setEnabled(b);
	}
	
	public void setPeakeState(boolean b)
	{
		btnPeak.setEnabled(b);
	}
	
	public void setEmptyState(boolean b)
	{
		btnEmpty.setEnabled(b);
	}
	
	public void addStopListener(ActionListener a)
	{
		btnStop.addActionListener(a);
	}
}
