package Stores;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

public class Queue extends Thread{
	private ArrayList<Client> clients;
	private int totalWaitingTime;
	private int totalNumberClients;
	private int totalServiceTime;
	private int emptyTime;
	private int number;
	private boolean open;
	private int currentTime;
	private int remainingTime;
	private int closeTime;
	private int endTime;
	private BufferedWriter out;
	
	public Queue(int number, int time, int begin, BufferedWriter out)
	{
		this.out = out;
		clients = new ArrayList<Client>();
		this.number = number;
		currentTime = begin;
		totalWaitingTime = 0;
		totalNumberClients = 0;
		totalServiceTime = 0;
		emptyTime = 0;
		open = false;
		remainingTime = time;
		endTime = time;
		closeTime = 0;
	}
	
	public void run()
	{
		try{
			while(remainingTime > 0 || clients.isEmpty() == false)
			{
				String s = removeClient();
				out.write(s);
				out.newLine();
			}
			if(getOpen() == true)
			{
				out.write(setOpen(false));
				out.newLine();
				closeTime = endTime + 10;
			}
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized String addClient(Client c, int time)
	{
			if(clients.isEmpty())
			{
				remainingTime -= time - currentTime;
				emptyTime += time - currentTime;
				currentTime = time;
			}
			c.setDepartingTime(this.getWaitingTime() + c.getServiceTime() + c.getArrivingTime());
			totalNumberClients++;
			totalWaitingTime += c.getWaitingTime();
			totalServiceTime += c.getServiceTime();
			clients.add(c);
			notifyAll();
			return c.toString() + " waits in queue number " + this.number + ". " + "Service time is "+ c.getServiceTime() + ". ";
	}
	
	public synchronized String removeClient() throws InterruptedException, IOException
	{
		if(clients.isEmpty())
		{
			out.write(setOpen(false));
			out.newLine();
			closeTime = currentTime;
			wait();
		}
		remainingTime -= clients.get(0).getServiceTime();
		currentTime += clients.get(0).getServiceTime();
		for(int i = 0; i < clients.get(0).getServiceTime() * 10; i++)
			wait(100);
		Client c = clients.get(0);
		clients.remove(0);
		notifyAll();
		return c.toString() + " has been served. The time is: " + currentTime;
	}
	
	public int getWaitingTime()
	{
		int waitingTime = 0;
		for(Client c : clients)
		{
			waitingTime += c.getServiceTime();
		}
		return waitingTime;
	}
	
	
	public int getEmptyTime()
	{
		return emptyTime;
	}
	
	public void addEmptyTime(int t)
	{
		emptyTime += t;
	}
	
	public int getServiceTime()
	{
		return totalServiceTime;
	}
	
	public int getCloseTime()
	{
		return closeTime;
	}
	
	
	public int getTotalWaiting()
	{
		return totalWaitingTime;
	}
	
	public int getTotalClients()
	{
		return totalNumberClients;
	}
	
	public int getNoClients()
	{
		return clients.size();
	}
	
	public synchronized String setOpen(boolean b)
	{
		open = b;
		if(b == false)
			return "The queue " + number + " is closed.";
		return "The queue " + number + " is open.";
	}
	
	public boolean getOpen()
	{
		return open;
	}
	
	public String toString()
	{
		String s = new String();
		for(Client c : clients)
			s = s.concat(c.getNumber() + " ");
		return s;
	}
}
