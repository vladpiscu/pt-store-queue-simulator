package Stores;
import java.util.Random;

public class Client {
	private static int globalNumber = 0;
	private int clientNumber;
	private int serviceTime; //serviceTime measures the time in seconds.
	private int arrivingTime;
	private int departingTime;
	
	public Client(int minServiceTime, int maxServiceTime, int arrive)
	{
		arrivingTime = arrive;
		Random rand = new Random();
		serviceTime = rand.nextInt(maxServiceTime - minServiceTime + 1) + minServiceTime;
		globalNumber++;
		clientNumber = globalNumber;
	}
	
	public static void resetGlobalNumber()
	{
		globalNumber = 0;
	}
	
	public int getArrivingTime()
	{
		return arrivingTime;
	}
	
	public void setDepartingTime(int d)
	{
		departingTime = d;
	}
	
	public int getServiceTime()
	{
		return serviceTime;
	}
	
	public int getWaitingTime()
	{
		return departingTime - serviceTime - arrivingTime;
	}
	
	public int getNumber()
	{
		return clientNumber;
	}
	
	public String toString()
	{
		return "Client " + clientNumber;
	}
	
	
}
