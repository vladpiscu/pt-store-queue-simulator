package Stores;

import GUI.*;

public class VisualQueues extends Thread{
	private Queue[] queues;
	private String s;
	private GUI g;
	
	public VisualQueues(Queue[] q, GUI g)
	{
		queues = q;
		this.g = g;
	}
	
	public void run()
	{
		boolean b = true;
		while(b)
		{
			s = new String("");
			for(int i = 0; i < queues.length; i++)
			{
				if(queues[i].getOpen() == true)
					s = s.concat(" OPEN : ");
				else
					s = s.concat("CLOSED: ");
				s = s.concat("Queue " + i + ": " + queues[i].toString() + "\n");
			}
			try {
				g.setQueueEv(s);
				sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			b = false;
			for(int i = 0; i < queues.length; i++)
				if(queues[i].isAlive())
					b = true;
		}
		g.setStartState(true);
		s = new String("");
		for(int i = 0; i < queues.length; i++)
		{
			s = s.concat("CLOSED: ");
			s = s.concat("Queue " + i + ": " + "\n");
		}
		g.setQueueEv(s);
		
	}
}
