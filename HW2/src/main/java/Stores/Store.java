package Stores;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

public class Store extends Thread{
	private Queue[] queues;
	private int currentTime;
	private int minServiceTime;
	private int maxServiceTime;
	private int minInterval;
	private int maxInterval;
	private int simEnd;
	private int waitingThreshold;
	private int peak;
	private int peakClients;
	private BufferedWriter out;
	
	public Store(Queue[] queues, int minService, int maxService, int minInterval, int maxInterval, int simBegin, int simEnd, int waitThreshold, BufferedWriter out)
	{
		this.out = out;
		currentTime = simBegin;
		peak = simBegin;
		peakClients = 0;
		this.queues = queues;
		minServiceTime = minService;
		maxServiceTime = maxService;
		this.minInterval = minInterval;
		this.maxInterval = maxInterval;
		this.simEnd = simEnd;
		waitingThreshold = waitThreshold;
	}
	
	public synchronized void addClient(int time) throws InterruptedException, IOException
	{
		for(int i = 0; i < time * 10; i++)
			sleep(100);
		if(isFull() && getNoQueuesOpen() < queues.length)
		{
			openQueue();
		}
		Client c = new Client(minServiceTime, maxServiceTime, currentTime);
		String afis = getBestQueue().addClient(c, currentTime);
		out.write(afis + "The current time is: " + currentTime);
		out.newLine();
		if(getNoClients() > peakClients)
		{
			peakClients = getNoClients();
			peak = currentTime;
		}
	}
	
	@SuppressWarnings("deprecation")
	public void run()
	{
		try{
			Client.resetGlobalNumber();
			while(isSimEnd() == false)
			{					
				Random rand = new Random();
				int nextClientTime = rand.nextInt(maxInterval - minInterval + 1) + minInterval;
				currentTime += nextClientTime;
				if(isSimEnd() == false)
					addClient(nextClientTime);
			}
			while(this.isEmpty() == false)
			{
				sleep(100);
			}
			for(int i = 0; i < queues.length; i++)
			{
				if(simEnd > queues[i].getCloseTime())
					queues[i].addEmptyTime(simEnd - queues[i].getCloseTime());
				queues[i].stop();
			}
			out.flush();
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int getPeakTime()
	{
		return peak;
	}
	
	public int getPeakClients()
	{
		return peakClients;
	}
	
	private int getNoClients()
	{
		int k = 0;
		for(int i = 0; i < queues.length; i++)
		{
			k += queues[i].getNoClients();
		}
		return k;
	}
	
	private boolean isSimEnd()
	{
		if(currentTime >= simEnd)
			return true;
		return false;
	}
	
	private Queue getBestQueue()
	{
		Queue q = queues[0];
		for(int i = 1; i < queues.length; i++)
			if(q.getWaitingTime() > queues[i].getWaitingTime() && queues[i].getOpen())
				q = queues[i];
		return q;
	}
	
	public Queue[] getQueues()
	{
		return queues;
	}
	
	public int averageWaiting()
	{
		int sum = 0;
		int people = 0;
		for(int i = 0; i < queues.length; i++)
		{
			sum += queues[i].getTotalWaiting();
			people += queues[i].getTotalClients();
		}
		if(people == 0)
			return 0;
		return sum / people;
	}
	
	public int averageService()
	{
		int sum = 0;
		int people = 0;
		for(int i = 0; i < queues.length; i++)
		{
			sum += queues[i].getServiceTime();
			people += queues[i].getTotalClients();
		}
		if(people == 0)
			return 0;
		return sum / people;
	}
	
	public boolean isFull()
	{
		for(int i = 0; i < queues.length; i++)
		{
			if(queues[i].getWaitingTime() < waitingThreshold && queues[i].getOpen())
				return false;
		}
		return true;
	}
	
	private int getNoQueuesOpen()
	{
		int k = 0;
		for(int i = 0; i < queues.length; i++)
		{
			if(queues[i].getOpen())
				k++;
		}
		return k;
	}
	
	private boolean isEmpty()
	{
		for(int i = 0; i < queues.length; i++)
		{
			if(queues[i].getNoClients() > 0)
				return false;
		}
		return true;
	}
	
	private void openQueue() throws IOException
	{
		for(int i = 0; i < queues.length; i++)
		{
			if(queues[i].getOpen() == false)
			{
				out.write(queues[i].setOpen(true));
				out.newLine();
				return;
			}
		}
		
	}
	
	
	
	
	
}
