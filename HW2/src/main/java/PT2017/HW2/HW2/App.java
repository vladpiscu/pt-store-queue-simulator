package PT2017.HW2.HW2;

import Stores.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import GUI.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	GUI g = new GUI();
    	g.setVisible(true);
    	Controller c = new Controller(g);
    }
}
